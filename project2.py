#Name:Gorthi Venkata Srinivas Pavan Krishna
#Lab Assignment 9
#CS61002-AP1
#04/19/2016

import math

class Snow_person:
    # Initilaizing vector
    def __init__(self,x,y):
        self.x = float(x)
        self.y = float(y)

    # Returning a string        
    def __str__(self):
        return str(self.x)+','+str(self.y)

    # Adding a vector with other vector
    def __add__(self,myvector):
        return MyVector(myvector.x + self.x,myvector.y + self.y)


class Snow_man:
    # Initilaizing vector
    def __init__(self,x,y):
        self.x = float(x)
        self.y = float(y)

    # Returning a string        
    def __str__(self):
        return str(self.x)+','+str(self.y)

    # Adding a vector with other vector
    def __add__(self,myvector):
        return MyVector(myvector.x + self.x,myvector.y + self.y)    
    
class Snow_lady:
    # Initilaizing vector
    def __init__(self,x,y):
        self.x = float(x)
        self.y = float(y)

    # Returning a string        
    def __str__(self):
        return str(self.x)+','+str(self.y)

    # Adding a vector with other vector
    def __add__(self,myvector):
        return MyVector(myvector.x + self.x,myvector.y + self.y)
    
class Line:
    # Initilaizing vector
    def __init__(self,x,y):
        self.x = float(x)
        self.y = float(y)
        lineObject = turtle.Turtle()

    # Returning a string        
    def __str__(self):
        return str(self.x)+','+str(self.y)

    # Adding a vector with other vector
    def __add__(self,myvector):
        lineObject.forward(50)

class MyRectangle(turtle.Turtle):

    def __init__(self):
        """Turtle Constructor"""
        turtle.Turtle.__init__(self)
 
    #----------------------------------------------------------------------
    def drawRectangle(self, x, y):
        """
        Moves the turtle to the correct position and draws a circle
        """
        self.penup()
        self.color(random.random(),random.random(), random.random())                
        self.pendown()
        self.forward(x)          # Tell alex to move forward by 50 units
        self.left(90)             # Tell alex to turn by 90 degrees
        self.forward(y)  
        self.left(90)
        self.forward(x)  
        self.left(90)        
        self.forward(y) 
    
class Circle:
    # Initilaizing vector
    def __init__(self,x,y):
        self.x = float(x)
        self.y = float(y)

    # Returning a string        
    def __str__(self):
        return str(self.x)+','+str(self.y)
    
    def drawCircle(x,col,pencol):
        turtle.penup()
        turtle.right(90)
        turtle.forward(x)
        turtle.right(270)
        turtle.pendown()
        turtle.color(col)
        turtle.pencolor(pencol)
        turtle.begin_fill()
        turtle.circle(x)
        turtle.end_fill()
        turtle.penup()
        turtle.home()
        
    def drawACircle(self, x, y, z):
        """
        Moves the turtle to the correct position and draws a circle
        """
        self.penup()
        self.setposition(x, y)
        self.color(random.random(),random.random(), random.random())        
        self.pendown()
#        self.color(color)
        turtle.begin_fill()
        self.circle(z)
        turtle.end_fill()
    
c = MyVector(3,4)#Creating an object c 
d = MyVector(5,6)#Creating an object d 

class Triangle:
    # Initilaizing vector
    def __init__(self,x,y):
        self.x = float(x)
        self.y = float(y)

    # Returning a string        
    def __str__(self):
        return str(self.x)+','+str(self.y)

    # Adding a vector with other vector
    def __add__(self,myvector):
        return MyVector(myvector.x + self.x,myvector.y + self.y)    
    
c = MyVector(3,4)#Creating an object c 
d = MyVector(5,6)#Creating an object d 

print "The first vector is:",c#printing first vector
print "The second vector is:",d#printing second vector

print "The addition of two vectors is:",c.__add__(d)#Calling a method to add a vector with other vector
print "The subtraction of two vectors is:",c.__sub__(d)#Calling a method to subtract a vector with other vector
print "The product of two vectors is:",c.__mul__(4)#Calling a method to multiply with a value 
print "The dot product of two vectors is:",c.__dotproduct__(d)#Calling a method to calculate dot product of a vector
print "The magnitude of two vectors is:",c.__mag__()#Calling a method to find magnitude of a vector













































    