#  Venkata Srinivas Pavan Krishna Gorthi 
# MS in DS
# CS 21163

import turtle
import random

class MyCirclee(turtle.Turtle):

    def __init__(self):
        """Turtle Constructor"""
        turtle.Turtle.__init__(self)
 
    #----------------------------------------------------------------------
    def drawCircle(self, x, y, z):
        """
        Moves the turtle to the correct position and draws a circle
        """
        self.penup()
        self.setposition(x, y)
        self.color(random.random(),random.random(), random.random())        
        self.pendown()
#        self.color(color)
        turtle.begin_fill()
        self.circle(z)
        turtle.end_fill()

            

class MyRectangle(turtle.Turtle):

    def __init__(self):
        """Turtle Constructor"""
        turtle.Turtle.__init__(self)
 
    #----------------------------------------------------------------------
    def drawRectangle(self, x, y):
        """
        Moves the turtle to the correct position and draws a circle
        """
        self.penup()
        self.color(random.random(),random.random(), random.random())                
        self.pendown()
        self.forward(x)          # Tell alex to move forward by 50 units
        self.left(90)             # Tell alex to turn by 90 degrees
        self.forward(y)  
        self.left(90)
        self.forward(x)  
        self.left(90)        
        self.forward(y)  
            
if __name__ == "__main__":
    t = MyRectangle()
    length = int(input("Enter the length of rectangle:")) 
    breadth = int(input("Enter the length of rectangle:")) 
    t.drawRectangle(length,breadth)
    turtle.getscreen()._root.mainloop()